# MyNXT basic plugin

This plugin demonstrates some of the basic MyNXT plugin functionality.

## Dependencies

To develop a plugin we recommend the following tools:

- [NPM](https://docs.npmjs.com/getting-started/installing-node)
- [Grunt](http://gruntjs.com/getting-started)
- [Bower](http://bower.io/)

## Getting started

To get started run the following commands

- `npm install`
- `bower install`
- `grunt`