$(document).ready(function () {
  var mainAccount = '';

  MyNXT.getAccounts(function (result) {
    if(result && !result.errorCode) {

      for (var i = 0; i < result.length; i++) {
        var account = result[i];

        if(account.bl_selected == 1) {
          mainAccount = account;
        }
      }

      $("#accountExample").find('.accounts').html(JSON.stringify(result));
    }
  });

  var nrsExample = $('#nrsExample');

  nrsExample.find('button[type=submit]').on('click', function () {
    var transactionId = nrsExample.find('[name=transactionId]').val();

    var data = {
      transaction: transactionId
    };

    MyNXT.nrsRequest('getTransaction', data, function (result) {
      nrsExample.find('.transaction').html(JSON.stringify(result));
    });
  });

  var transactionExample = $('#transactionExample');

  transactionExample.find('button[type=submit]').on('click', function () {
    var amountNQT = transactionExample.find('[name=amountNQT]').val();
    var recipient = transactionExample.find('[name=recipient]').val();
    var accountToUse = transactionExample.find('[name=account]').val();

    var data = {
      requestType: 'sendMoney',
      amountNQT: amountNQT,
      recipient: recipient,
      account: accountToUse
    };

    MyNXT.sendTransaction(data, function (result) {
      transactionExample.find('.result').html(JSON.stringify(result));
    });
  });

  var websiteTokenExample = $('#websiteTokenExample');

  websiteTokenExample.find('button[type=submit]').on('click', function () {
    var website = websiteTokenExample.find('[name=website]').val();
    var accountToUse = websiteTokenExample.find('[name=account]').val();

    var data = {
      requestType: 'generateToken',
      website: website,
      account: accountToUse
    };

    MyNXT.sendTransaction(data, function (result) {
      websiteTokenExample.find('.result').html(JSON.stringify(result));
    });
  });
});